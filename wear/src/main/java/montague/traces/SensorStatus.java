package montague.traces;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import montague.traces.services.SensorService;

public class SensorStatus extends Activity {


    TextView infoText;
    Button configButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_status);

        infoText = (TextView)findViewById(R.id.info_text);
        configButton = (Button)findViewById(R.id.config_btton);
        //todo remove
        configButton.setText("Check Status");
        configButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStop();
            }
        });

//        Intent i = new Intent(SensorStatus.this, SensorService.class);
//        i.putExtra(SensorService.EXTRA_TIMESTAMP, LoggerUtils.dateTimeStamp(System.currentTimeMillis()));
//        if(SensorStatus.this.startService(i)!=null){
//            Log.d("SERVICE","STARTED");
//        }
        checkStatus();
    }

    private void startStop() {
        checkStatus();
    }


    private void checkStatus(){
        int stringResource = (isMyServiceRunning(SensorService.class))?R.string.status_enabled:R.string.status_disabled;
        if(infoText!=null){
            infoText.setText(stringResource);
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            Log.d("SERVICE",serviceClass.getName() +" ? "+ service.service.getClassName());
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
