package montague.traces;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.view.WearableListView;

import preference.WearPreferenceActivity;

/**
 * Created by kyle montague on 17/11/15.
 */
public class SensorPreferenceActivity extends WearPreferenceActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.sensor_preferences);
    }

    final int SAVE_POS = 0;

    @Override
    public void onClick(WearableListView.ViewHolder viewHolder) {
        super.onClick(viewHolder);
        if (viewHolder.getAdapterPosition() == SAVE_POS) {
            //here we should restart the sensors object (i.e. start/stop the sensors based on these changes.)
            finish();
        }
    }


}
