package montague.traces.storage;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by kylemontague on 22/07/15.
 */
public class LoggerUtils {

    public static long dateTimeStamp(long timestamp){

        //If you want the current timestamp :
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(timestamp));
        c.set(Calendar.HOUR,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        return c.getTime().getTime();
    }
}
