package montague.traces.activities;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;

import montague.traces.R;
import montague.traces.utilities.Network;

/**
 * Created by kylemontague on 05/01/16.
 */
public class HelpActivity extends Activity{


    WebView mWebview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_screen);

        Uri data = getIntent().getData();



        mWebview = (WebView)findViewById(R.id.webview);
        if(mWebview!=null){
            if(Network.isNetworkOnline(getApplicationContext()) || data == null)
                mWebview.loadUrl(data.toString());
            else
                mWebview.loadData("<html><body><H1>Error: No Network Connection</H1><p>check the internet connection then try again.</p></body></html>","text/html","UTF-8");
        }
    }
}
