package montague.traces.storage;

import android.os.Environment;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

/**
 * Created by kylemontague on 06/08/15.
 */
public class FileUtils {

    public static String APP_DIR = Environment.getExternalStorageDirectory().getPath()+"/Traces";

    public static ArrayList<String> listAllSubFolders(){
        File dir = new File(APP_DIR);
        ArrayList<String> folders = new ArrayList<>();
        if(dir.exists()) {
            File[] fList = dir.listFiles();
            for (File file : fList) {
                if (file.isDirectory()) {
                    String[] data = listAllZipFiles(file.getAbsolutePath());
                    if (data != null && data.length > 0)
                        folders.add(file.getAbsolutePath());
                }
            }
        }
        return folders;
    }


    public static String[] listAllZipFiles(){
        File dir = new File(APP_DIR);
        if(!dir.exists())
            return null;

        return dir.list(new ZipFilenameFilter());
    }

    public static String[] listAllZipFiles(String directory){
        File dir = new File(directory);
        if(!dir.exists())
            return null;

        return dir.list(new ZipFilenameFilter());
    }


    public static class ZipFilenameFilter implements FilenameFilter {

        public boolean accept(File dir, String name) {
            if (name == null) {
                return false;
            }
            return name.toLowerCase().endsWith(".zip");
        }
    }


}
