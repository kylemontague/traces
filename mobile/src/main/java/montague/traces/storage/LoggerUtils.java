package montague.traces.storage;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by kylemontague on 22/07/15.
 */
public class LoggerUtils {

    public static long dateTimeStamp(long timestamp){

        //If you want the current timestamp :
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(timestamp));
        c.set(Calendar.HOUR,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);
        return c.getTime().getTime();
    }


    public static String getName(Context context){
        // Reconstitute the pairing device name from the model and the last 4 digits of the bluetooth MAC
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String wearName = sp.getString("DEVICE_NAME",null);
        if(wearName == null) {
            BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
            String btAddress = "No Bluetooth";
            if (btAdapter != null)
                btAddress = btAdapter.getAddress();
            if ((btAddress != null) && (!btAddress.equals("No Bluetooth"))) {
                wearName = android.os.Build.MODEL;
                String[] tokens = btAddress.split(":");
                wearName += " " + tokens[4] + tokens[5];
                wearName = wearName.toUpperCase();
                sp.edit().putString("DEVICE_NAME",wearName).apply();
            } else {
                wearName = "No Bluetooth";
            }
        }
        return wearName;
    }
}
