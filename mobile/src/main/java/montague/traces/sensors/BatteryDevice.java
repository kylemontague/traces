package montague.traces.sensors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import montague.traces.storage.Logger;

/**
 * Created by kyle montague on 10/05/15.
 */
public class BatteryDevice extends BroadcastReceiver {

    private static BatteryDevice mShared;
    public static BatteryDevice shared(Context context, long timestamp){
        if(mShared == null)
            mShared = new BatteryDevice(context, timestamp);
        return mShared;
    }


    boolean isRegistered = false;
    Context mContext;
    Logger mLogger;
    static String Battery = "Battery";

    public BatteryDevice(Context context, long timestamp){
        mContext = context;
        mLogger = new Logger(BatteryDevice.Battery,20,timestamp, Logger.FileFormat.csv);
    }

    public void start(){
        mContext.registerReceiver(this,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        isRegistered = true;
    }

    public void stop(){
        if(isRegistered) {
            mContext.unregisterReceiver(this);
            isRegistered = false;
        }
        mLogger.flush();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int batteryLevel = intent.getIntExtra(
                BatteryManager.EXTRA_LEVEL, 0);
        int maxLevel = intent
                .getIntExtra(BatteryManager.EXTRA_SCALE, 0);
        int batteryHealth = intent.getIntExtra(
                BatteryManager.EXTRA_HEALTH,
                BatteryManager.BATTERY_HEALTH_UNKNOWN);
        int temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
        float tempTwo = ((float) temp) / 10;
        //float batteryPercentage = ((float) batteryLevel / (float) maxLevel) * 100;
        mLogger.writeAsync(System.currentTimeMillis()+","+batteryLevel+","+tempTwo+","+batteryHealth);
    }


    public String getFilename(){
        return mLogger.getFilename();
    }



}
