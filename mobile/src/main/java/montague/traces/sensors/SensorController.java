package montague.traces.sensors;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Kyle Montague on 10/05/15.
 */
public class SensorController {

    BLEDevice mBLE;
    MotionDevice mMotion;
    BatteryDevice mBattery;
    MagnetDevice mMagnet;
    TemperatureDevice mTemperture;
//    AmbientNoiseDevice mNoise;

    private final long AUDIO_SAMPLE_DELAY = 1000*60*5; //5 minutes



    public SensorController(Context context, long timestamp){
        mBLE = BLEDevice.shared(context,timestamp);
        mMotion = MotionDevice.shared(context,timestamp);
        mBattery = BatteryDevice.shared(context,timestamp);
        mMagnet = MagnetDevice.shared(context,timestamp);
        mTemperture = TemperatureDevice.shared(timestamp);
//        mNoise = AmbientNoiseDevice.shared(timestamp);
    }

    public void start(){
        mBLE.start();
        mMotion.start();
        mBattery.start();
        mMagnet.start();
//        mNoise.startSampling(AUDIO_SAMPLE_DELAY);

    }

    public void stop(){
        mBLE.stop();
        mMotion.stop();
        mBattery.stop();
        mMagnet.stop();
//        mNoise.stopSampling();
        mTemperture.stop();
    }


    public ArrayList<String> getFiles(){
        ArrayList<String> tmp = new ArrayList<String>();
        tmp.add(mBLE.getFilename());
        tmp.add(mMotion.getAccFilename());
        tmp.add(mMotion.getGyroFilename());
        tmp.add(mMagnet.getFilename());
        tmp.add(mBattery.getFilename());
        tmp.add(mTemperture.getFilename());
//        tmp.add(mNoise.getFilename());
        return tmp;
    }
}
