package montague.traces.sensors;

import montague.traces.storage.Logger;

/**
 * Created by kylemontague on 10/11/15.
 */
public class TemperatureDevice {

    private static final String TAG = "ESTIMOTE";
    private static TemperatureDevice mShared;
    public static TemperatureDevice shared(long timestamp){
        if(mShared==null){
            mShared = new TemperatureDevice(timestamp);
        }
        return mShared;
    }

    public TemperatureDevice(long timestamp){
        mLogger = new Logger(NAME,500,timestamp, Logger.FileFormat.csv);
    }

    public static String NAME = "TEMPERATURE";
    private Logger mLogger;

    public void addTemp(long timestamp, String MAC, float temperature, long elapsedTime, int batteryVoltage){
        mLogger.writeAsync(timestamp+","+MAC+","+temperature+","+elapsedTime+","+batteryVoltage);
    }


    public void stop(){
        mLogger.flush();
    }

    public String getFilename(){
        return mLogger.getFilename();
    }
}
